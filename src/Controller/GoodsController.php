<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Goods;

class GoodsController extends AbstractController
{
    /**
     * @Route("/goods", name="goods")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Goods::class);
        $products = $repository->findAll();
        foreach($products as $val)
        {
            //$picture[] =base64_encode(stream_get_contents($val->getPicture()));
            $picture[] =base64_encode(stream_get_contents($val->getPicture()));

            //echo '<img src="data:image/jpg;base64,' .  base64_encode($image)  . '" />';
        }
        return $this->render('goods/index.html.twig', [
            'goods' => $products,
            'pics' => $picture
        ]);
    }
}
