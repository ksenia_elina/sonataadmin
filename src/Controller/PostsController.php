<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Posts;

class PostsController extends AbstractController
{
    /**
     * @Route("/posts", name="posts")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Posts::class);
        $products = $repository->findAll();
        foreach($products as $val)
        {
            //$picture[] =base64_encode(stream_get_contents($val->getPicture()));
            $picture[] =base64_encode(stream_get_contents($val->getPicture()));

            //echo '<img src="data:image/jpg;base64,' .  base64_encode($image)  . '" />';
        }
        return $this->render('posts/index.html.twig', [
            'posts' => $products,
            'pics' => $picture
        ]);
    }
}
